CREATE TABLE [dbo].[Publications2]
(
	[PUB_ID] [int] primary key NOT NULL,
	[PUB_Title] [varchar](MAX) NULL,
	[PUB_Publisher] [varchar](MAX) NULL,
	[PUB_Source] [varchar](MAX) NULL 
);

CREATE TABLE [dbo].[Authors]
(
	[AUT_ID] [int] primary key NOT NULL,
	[AUT_IDENTITY] [varchar](255) NULL,
	[AUT_Email] [varchar](255) NULL,
	[AUT_Logo] [varchar](1055) NULL,
	[AUT_Address] [varchar](255) NULL
);

CREATE TABLE [dbo].[AuthorsPublications]
(
	[AUP_ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[AUP_AuthorID] [int] NULL,
	[AUP_PublicationID] [int] NULL
);

CREATE TABLE [dbo].[MNiSWList]
(
	[MSW_ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[MSW_DOCID] [int] NULL,
	[MSW_INNERID] [int] NULL,
	[MSW_PublicationName] [varchar](255) NULL,
	[MSW_PublicationISSN] [varchar](255) NULL,
	[MSW_PublicationEISSN] [varchar](255) NULL,
	[MSW_Points] [int] NULL,
	[MSW_PublicationNameTags] [varchar](255) NULL
);



--Scripts

GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeSummary]    Script Date: 2019-06-16 20:02:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetEmployeeSummary] 
	-- Add the parameters for the stored procedure here
	@p2 varchar(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 SELECT PUB_ID
       ,PUB_Title
       ,PUB_Source
	,a.AUT_ID
    ,a.AUT_IDENTITY
	,a.AUT_Email
	,a.AUT_Logo
	,a.AUT_Address
	,m.MSW_Points
	,m.MSW_PublicationName 
    FROM Publications2 
	LEFT JOIN AuthorsPublications ap on PUB_ID = ap.AUP_PublicationID
    LEFT JOIN Authors a on ap.AUP_AuthorID = a.AUT_ID 
    LEFT JOIN MNiSWList m on PUB_Source like m.MSW_PublicationName or PUB_Source like m.MSW_PublicationNameTags
    WHERE a.AUT_IDENTITY  like ISNULL ('%' + @p2 + '%', a.AUT_IDENTITY)
  
END



GO
/****** Object:  StoredProcedure [dbo].[GetPublications]    Script Date: 2019-06-15 18:27:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPublications] 
	-- Add the parameters for the stored procedure here
	@RowsPerPage  int = 50,
	@OffsetRows int = 1,
	@source varchar(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

IF @source IS NOT NULL
	SELECT *
	FROM Publications2
	WHERE PUB_Source like  ISNULL ('%' + @source + '%', PUB_Source)
	ORDER BY PUB_ID
	OFFSET @OffsetRows * @RowsPerPage ROWS
	FETCH NEXT @RowsPerPage ROWS ONLY
ELSE
    SELECT *
	FROM Publications2
	ORDER BY PUB_ID
	OFFSET @OffsetRows * @RowsPerPage ROWS
	FETCH NEXT @RowsPerPage ROWS ONLY


END