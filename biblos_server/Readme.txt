Wymagania:
Python 3.7
Microsoft SQL Server >= 2017
ManagamentStudio >= 2017
JAVA :'c

1. Pobranie repo do ustalonego folderu 
2. Otworzenie projektu i stworzenie virtual env (w folderze poza projektem!).
   * pip install virtualenv
   * virtualenv <path>
	np:(virtualenv C:\Users\Arek\Desktop\biblos\biblos_server\venv)
3. Uruchomienie virtual env
   Komenda: <path>/activate
	np:(C:\Users\Arek\Desktop\biblos\biblos_server\venv\Scripts\activate)
4. Instalacja wymaganych paczek:
   Komenda: pip install -r requirements.txt
5. Konfiguracja interpretera, dodac interpreter tj. na png'ku add_python_interpreter.png
6. Stworzenie lokanej bazy danych:
   Poprzez managament studio, nazwa db <Biblos>
7. Uruchomienie skrypt�w: skrypty.sql
8. Zmiana connection string w app.python na w�asny (wystarczy tylko linijk�: Server=DESKTOP-S9S847J\SQLEXPRESS)
9. Gotowe.

