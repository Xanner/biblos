from Models import name_helper
from Presistence.Readers.pdf_reader import PDFReader
from Presistence.Readers.sql_reader import SQLReader
from Presistence.Readers.xml_reader import XMLReader


def create_reader(type, conn_string="Not Required"):
    if type == name_helper.Reader.SQL:
        return SQLReader(conn_string)
    elif type == name_helper.Reader.PDF:
        return PDFReader()
    elif type == name_helper.Reader.XML:
        return XMLReader()
    else:
        raise ValueError("Not implemented reader: " + type)





    