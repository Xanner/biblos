from Models import name_helper
from Presistence.Factories import reader_simple_factory
from Presistence.Repositories.biblos_data_repository import BiblosDataRepository
from Presistence.Repositories.mnisw_list_repository import MNiSWListRepository


class UnitOfWork:

    connection_string = None

    @staticmethod
    def biblos_data_repository():
        return BiblosDataRepository(reader_simple_factory.create_reader(name_helper.Reader.SQL,
                                                                        UnitOfWork.connection_string),
                                    reader_simple_factory.create_reader(name_helper.Reader.XML))

    @staticmethod
    def mnisw_list_repository():
        return MNiSWListRepository(reader_simple_factory.create_reader(name_helper.Reader.SQL,
                                                                       UnitOfWork.connection_string),
                                   reader_simple_factory.create_reader(name_helper.Reader.PDF))

    @staticmethod
    def save_changes():
        reader_simple_factory.create_reader(name_helper.Reader.SQL, UnitOfWork.connection_string).commit()