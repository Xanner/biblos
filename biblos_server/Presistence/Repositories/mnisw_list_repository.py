from typing import List

from Presistence.Readers.sql_reader import SQLReader
from Presistence.Readers.pdf_reader import PDFReader
from Models.mnisw_record import MniswRecord


class MNiSWListRepository:

    def __init__(self, sql_reader: SQLReader, pdf_reader: PDFReader):
        self.sql_reader = sql_reader
        self.pdf_reader = pdf_reader

    def read_data_from_pdf(self, path, area, pages, spreadsheet_mode, panda_options, oIndex):
        mnisw_records_raw = self.pdf_reader.read_pdf(path, area, pages, spreadsheet_mode, panda_options, oIndex)
        mnisw_records = []

        for val in mnisw_records_raw:
            mnisw_records.append(MniswRecord.MniswRecordBuilder().set_id(val[0])
                                                                 .set_name(val[1])
                                                                 .set_issn(val[2])
                                                                 .set_eissn(val[3])
                                                                 .set_points(val[4]))
        return mnisw_records

    def save_data_in_database(self, mniswRecords: List[type(MniswRecord)]):
        sqlQuery = "INSERT INTO MNiSWList(MSW_DOCID, MSW_INNERID, MSW_PublicationName, MSW_PublicationISSN, " \
                   "MSW_PublicationEISSN, MSW_Points) VALUES (?, ?, ?, ?, ?, ?)"

        for value in mniswRecords:
            slqValue = (1, value.id, value.name, value.issn, value.eissn, value.points)
            self.sql_reader.execute_query(sqlQuery, slqValue)

    def remove_data_from_database(self):
        sql_truncate_query = "TRUNCATE TABLE MNiSWList"
        self.sql_reader.execute_query(sql_truncate_query, None)

    def read_data_from_database(self):
        mnisw_list = []
        sqlQuery = "SELECT * FROM [Biblos].[dbo].[MNiSWList]"

        data = self.sql_reader.execute_read_query(sqlQuery, None)
        for val in data:
            mnisw_list.append(MniswRecord.MniswRecordBuilder().set_id(val[0])
                                                              .set_name(val[3])
                                                              .set_issn(val[4])
                                                              .set_eissn(val[5])
                                                              .set_points(val[6]))

        return mnisw_list