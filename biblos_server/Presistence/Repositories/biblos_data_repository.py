import bleach

from Models.employee_summary import EmployeeSummary
from Models.publication import Publication
from Models.author import Author
from Models.author_publication import AuthorPublication
from Models.publication_page import PublicationPage
from Presistence.Readers.xml_reader import XMLReader
from Presistence.Readers.sql_reader import SQLReader


class BiblosDataRepository:

    def __init__(self, sql_reader: SQLReader, xml_reader: XMLReader):
        self.sql_reader = sql_reader
        self.xml_reader = xml_reader
        self.xml_reader.encode = 'iso-8859-1'

    def read_data_from_oai_api(self, oai_init_url, oai_url):
        self.firstIteration = True
        self.p_index = 1
        self.a_index = 1
        self.publications = []
        self.authors = dict()
        self.authors_publications = []

        while True:
            if self.firstIteration:
                data = self.xml_reader.load_data(oai_init_url)
                metadata = self.xml_reader.load_metaData(data, 'metadata')
            else:
                token = data.find('resumptiontoken').string
                if token is None:
                    return (self.publications, self.authors, self.authors_publications)
                next_url = oai_url + token
                print(next_url)
                data = self.xml_reader.load_data(next_url)
                metadata = self.xml_reader.load_metaData(data, 'metadata')

            for mData in metadata:
                title = mData.find('dc:title')
                creators = mData.findAll('dc:creator')
                publisher = mData.find('dc:publisher')
                source = mData.find('dc:source')

                parsed_creators = []
                parsed_title = self.xml_reader.parse_CData(title)
                parsed_source = self.xml_reader.parse_CData(source)

                parsed_publisher = self.xml_reader.parse_CData(publisher)
                if parsed_publisher is not None:
                    parsed_publisher = bleach.clean(parsed_publisher, tags=[], strip=True)

                for creator in creators:
                    parsed_creator = self.xml_reader.parse_CData(creator)
                    parsed_creators.append(parsed_creator)
                    if parsed_creator in self.authors:
                        self.authors_publications.append(
                            AuthorPublication.AuthorPublicationBuilder().set_author_id(self.authors[parsed_creator].id)
                                                                        .set_publication_id(self.p_index)
                                                                        .build())

                    else:
                        self.authors[parsed_creator] = Author.AuthorBuilder().set_id(self.a_index)\
                                                                             .set_identity(parsed_creator)\
                                                                             .build()

                        self.authors_publications.append(
                            AuthorPublication.AuthorPublicationBuilder().set_author_id(self.a_index)
                                                                        .set_publication_id(self.p_index)
                                                                        .build())

                        self.a_index += 1

                self.publications.append(Publication.PublicationBuilder().set_id(self.p_index)
                                                                         .set_title(parsed_title)
                                                                         .set_publisher(parsed_publisher)
                                                                         .set_source(parsed_source)
                                                                         .build())
                parsed_creators.clear()
                self.p_index += 1

            self.firstIteration = False

        return (self.publications, self.authors, self.authors_publications)


    def save_data_in_database(self, raw_data):
        sqlQuery = "INSERT INTO Publications2(PUB_ID, PUB_Title, PUB_Publisher, PUB_Source) VALUES (?, ?, ?, ?)"

        for i in range(0, len(raw_data[0]), 400):
            chunked_publications = raw_data[0][i:i + 400]
            print("Execute query from " + str(i) + " to " + str(i + 400))
            for pub in chunked_publications:
                slqValue = (
                    pub.id,
                    pub.title,
                    pub.publisher,
                    pub.source)
                self.sql_reader.execute_query(sqlQuery, slqValue)

        sqlQuery = "INSERT INTO Authors(AUT_ID, AUT_IDENTITY) VALUES (?, ?)"

        for aut in raw_data[1].values():
            sqlValue = (aut.id, aut.identity)
            self.sql_reader.execute_query(sqlQuery, sqlValue)

        sqlQuery = "INSERT INTO AuthorsPublications(AUP_AuthorID, AUP_PublicationID) VALUES (?, ?)"

        for aup in raw_data[2]:
            sqlValue = (aup.author_id, aup.publication_id)
            self.sql_reader.execute_query(sqlQuery, sqlValue)


    def get_employee_summary_form_database(self, employee):
        EmployeeSummaries = []

        sql = ("EXEC [dbo].[GetEmployeeSummary]"
		       "@p2 = ?")
        data = self.sql_reader.execute_read_query(sql, employee)

        for val in data:
            EmployeeSummaries.append(EmployeeSummary.EmployeeSummaryBuilder().set_publication_id(val[0])
                                                                             .set_publication_title(val[1])
                                                                             .set_publication_source(val[2])
                                                                             .set_author_id(val[3])
                                                                             .set_author_name(val[4])
                                                                             .set_author_email(val[5])
                                                                             .set_author_logo(val[6])
                                                                             .set_author_address(val[7])
                                                                             .set_mnisw_points(val[8])
                                                                             .build())


        return EmployeeSummaries


    def get_publications_from_database(self, page, source_fitration):
        offset_page = page - 1
        self.publications = []

        sql = ("EXEC [dbo].[GetPublications]"
		       "@RowsPerPage = 50,"
		       "@OffsetRows = ?,"
		       "@source = ?")

        sql_value = (offset_page, source_fitration)
        data = self.sql_reader.execute_read_query(sql, sql_value)

        for val in data:
            self.publications.append(Publication.PublicationBuilder().set_id(val[0])
                                                                     .set_title(val[1])
                                                                     .set_publisher(val[2])
                                                                     .set_source(val[3])
                                                                     .build())
        if source_fitration is not None:
            sql = ("SELECT COUNT(*) FROM Publications2 WHERE PUB_Source LIKE ISNULL ('%' + ? + '%', PUB_Source)")
        else:
            sql = ("SELECT COUNT(*) FROM Publications2")

        count_data = self.sql_reader.execute_read_query(sql, source_fitration)

        return PublicationPage.PublicationPageBuilder().set_count(count_data[0][0])\
                                                       .set_publications(self.publications)\
                                                       .build()

    def update_author_info(self, author):
        sql = "UPDATE Authors SET " \
              "AUT_Address = ?," \
              "AUT_Logo = ?," \
              "AUT_Email = ? " \
              "WHERE AUT_ID = ?"
        sql_value = (author.address, author.logo, author.email, author.id)

        self.sql_reader.execute_query(sql, sql_value)