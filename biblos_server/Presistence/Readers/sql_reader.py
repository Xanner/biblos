import pyodbc

class SingletonType(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(
                SingletonType, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class SQLReader(metaclass=SingletonType):

    def __init__(self, conn_string):
        self.initialize_connection(conn_string)

    def initialize_connection(self, connection_string):
        self.connectionString = connection_string
        self.connection = pyodbc.connect(self.connectionString)
        self.cursor = self.connection.cursor()

    def execute_query(self, query, value):
        sql_query = str(query)
        if value is not None:
            self.cursor.execute(sql_query, value)
        else:
            self.cursor.execute(sql_query)

    def execute_big_query(self, query, values):
        self.cursor.fast_executemany = True
        sql_query = str(query)
        self.cursor.executemany(sql_query, values)
        del values

    def execute_read_query(self, query, val):
        sql_query = str(query)
        if val is not None:
            return self.cursor.execute(sql_query, val).fetchall()
        else:
            return self.cursor.execute(sql_query).fetchall()

    def commit(self):
        self.connection.commit()