from typing import List
import tabula

class PDFReader:

    def read_pdf(self, path, area, pages, spreadsheet_mode, panda_options, oIndex):
        rawData = tabula.read_pdf(path, area,
                                  pages=pages, spreadsheet=spreadsheet_mode, pandas_options=panda_options)
        values: List = rawData.values.tolist()

        return values[oIndex:]
