from bs4 import BeautifulSoup, CData
from requests import get

class XMLReader:

    def __init__(self, encode="Default"):
        self._encode = encode

    @property
    def encode(self):
        return self._encode

    @encode.setter
    def encode(self, encode):
        self._encode = encode

    def parse_CData(self, data):
        if data != None:
            return data.find(text=lambda tag: isinstance(tag, CData)).string.strip()

    def load_data(self, url):
        html = get(url).text
        html = html.encode(self._encode)
        return BeautifulSoup(html, "html.parser")

    def load_metaData(self, data, meta_type):
        return data.find_all(meta_type)


