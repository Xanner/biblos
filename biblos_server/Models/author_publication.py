class AuthorPublication:
    def __init__(self, builder):
        self.author_id = builder.author_id
        self.publication_id = builder.publication_id

    class AuthorPublicationBuilder:
        def __init__(self):
            self.author_id = 0
            self.publication_id = 0

        def set_author_id(self, author_id):
            self.author_id = author_id
            return self

        def set_publication_id(self, publication_id):
            self.publication_id = publication_id
            return self

        def build(self):
            if self.author_id is None:
                raise ValueError('Author Id cannot be null')
            if self.author_id < 0:
                raise ValueError('Author Id cannot be less than 0')
            if self.publication_id is None:
                raise ValueError('Publication Id cannot be null')
            if self.publication_id < 0:
                raise ValueError('Publication Id cannot be less than 0')

            return AuthorPublication(self)