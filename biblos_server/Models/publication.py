class Publication:
    def __init__(self, builder):
        self.id = builder.id
        self.title = builder.title
        self.publisher = builder.publisher
        self.source = builder.source

    class PublicationBuilder:
        def __init__(self):
            self.id = 0
            self.title = None
            self.publisher = None
            self.source = None

        def set_id(self, id):
            self.id = id
            return self

        def set_title(self, title):
            self.title = title
            return self

        def set_publisher(self, publisher):
            self.publisher = publisher
            return self

        def set_source(self, source):
            self.source = source
            return self

        def build(self):
            if self.id is None:
                raise ValueError("Id cannot be null")
            if self.id < 0:
                raise ValueError("Id cannot be less than 0")

            return Publication(self)