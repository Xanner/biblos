class EmployeeSummary:
    def __init__(self, builder):
        self.publication_id = builder.publication_id
        self.publication_title = builder.publication_title
        self.publication_source = builder.publication_source
        self.author_name = builder.author_name
        self.author_address = builder.author_address
        self.author_id = builder.author_id
        self.author_logo = builder.author_logo
        self.author_email = builder.author_email
        self.mnisw_points = builder.mnisw_points

    class EmployeeSummaryBuilder:
        def __init__(self):
            self.publication_id = 0
            self.publication_title = None
            self.publication_source = None
            self.author_name = None
            self.author_address = None
            self.author_id = None
            self.author_logo = None
            self.author_email = None
            self.mnisw_points = 0

        def set_publication_id(self, publication_id):
            self.publication_id = publication_id
            return self

        def set_publication_title(self, publication_title):
            self.publication_title = publication_title
            return self

        def set_publication_source(self, publication_source):
            self.publication_source = publication_source
            return self

        def set_author_name(self, author_name):
            self.author_name = author_name
            return self

        def set_author_id(self, author_id):
            self.author_id = author_id
            return self

        def set_author_logo(self, author_logo):
            self.author_logo = author_logo
            return self

        def set_author_email(self, author_email):
            self.author_email = author_email
            return self

        def set_author_address(self, author_address):
            self.author_address = author_address
            return self

        def set_mnisw_points(self, mnisw_points):
            self.mnisw_points = mnisw_points
            return self

        def build(self):
            if self.publication_id is None:
                raise ValueError('Publication Id cannot be null')
            if self.mnisw_points is not None:
                if self.mnisw_points < 0:
                    raise ValueError('MniswPoints cannot be less than 0')

            return EmployeeSummary(self)