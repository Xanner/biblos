from typing import List


class Author:
    def __init__(self, builder):
        self.id = builder.id
        self.identity = builder.identity
        self.address = builder.address
        self.logo = builder.logo
        self.email = builder.email

    class AuthorBuilder:
        def __init__(self):
            self.id = 0
            self.identity = None
            self.address = None
            self.logo = None
            self.email = None

        def set_id(self, id):
            self.id = id
            return self

        def set_identity(self, identity):
            self.identity = identity
            return self

        def set_logo(self, logo):
            self.logo = logo
            return self

        def set_email(self, email):
            self.email = email
            return self

        def set_address(self, address):
            self.address = address
            return self

        def build(self):
            if self.id is None:
                raise ValueError('Id cannot be null')
            if self.id < 0:
                raise ValueError('Id cannot be less than 0')
            return Author(self)

    @staticmethod
    def deserialize_data(rawData):

        return Author.AuthorBuilder().set_id(rawData['author_id'])\
                                              .set_address(rawData['author_address'])\
                                              .set_identity(rawData['author_name'])\
                                              .set_logo(rawData['author_logo'])\
                                              .set_email(rawData['author_email'])\
                                              .build()


