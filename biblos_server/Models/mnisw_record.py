from typing import List

class MniswRecord:
    def __init__(self, builder):
        self.id = builder.id
        self.name = builder.name
        self.issn = builder.issn
        self.eissn = builder.eissn
        self.points = builder.points

    class MniswRecordBuilder:
        def __init__(self):
            self.id = 0
            self.name = None
            self.issn = None
            self.eissn = None
            self.points = 0

        def set_id(self, id):
            self.id = id
            return self

        def set_name(self, name):
            self.name = name
            return self

        def set_issn(self, issn):
            self.issn = issn
            return self

        def set_eissn(self, eissn):
            self.eissn = eissn
            return self

        def set_points(self, points):
            self.points = points
            return self

        def build(self):
            if self.id is None:
                raise ValueError("Id cannot be null")
            if int(self.points) < 0:
                raise ValueError("Points cannot be less than 0")

            return MniswRecord(self)

    @staticmethod
    def deserialize_data(rawData):
        data: List[type(MniswRecord)] = []

        for value in rawData:
            data.append(MniswRecord.MniswRecordBuilder().set_id(value['id'])
                                                        .set_name(value['name'])
                                                        .set_issn(value['issn'])
                                                        .set_eissn(value['eissn'])
                                                        .set_points(value['points'])
                                                        .build())
        return data