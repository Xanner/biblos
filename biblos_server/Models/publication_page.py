class PublicationPage:
    def __init__(self, builder):
        self.count = builder.count
        self.publications = builder.publications

    class PublicationPageBuilder:
        def __init__(self):
            self.count = 0
            self.publications = []

        def set_count(self, count):
            self.count = count
            return self

        def set_publications(self, publications):
            self.publications = publications
            return self

        def build(self):
            if self.count < 0:
                raise ValueError("Count cannot be less than 0")

            return PublicationPage(self)