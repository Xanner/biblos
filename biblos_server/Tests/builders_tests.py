from unittest import TestCase

from Models.author import Author
from Models.author_publication import AuthorPublication
from Models.employee_summary import EmployeeSummary
from Models.mnisw_record import MniswRecord
from Models.publication import Publication


class TestAuthorBuilder(TestCase):
    def setUp(self):
        self.author = None
        self.author_builder = Author.AuthorBuilder()


class TestAuthorPublicationBuilder(TestCase):
    def setUp(self):
        self.author_publication = None
        self.author_publication_builder = AuthorPublication.AuthorPublicationBuilder()


class TestEmployeeSummaryBuilder(TestCase):
    def setUp(self):
        self.empolyee_summary = None
        self.employee_summary_builder = EmployeeSummary.EmployeeSummaryBuilder()


class TestMniswRecordBuilder(TestCase):
    def setUp(self):
        self.mnisw_record = None
        self.mnisw_record_builder = MniswRecord.MniswRecordBuilder()


class TestPublicationBuilder(TestCase):
    def setUp(self):
        self.publication = None
        self.publication_builder = Publication.PublicationBuilder()



class TestAuthorBuilderIfCorrectShouldBuild(TestAuthorBuilder):
    def test_build_function(self):
        self.author = self.author_builder.set_id(1)\
                          .set_identity('Kowalski, Jan')\
                          .build()

        self.assertEqual(self.author.id, 1)
        self.assertEqual(self.author.identity, 'Kowalski, Jan')


class TestAuthorBuilderIfNotCorrectShouldTrowException(TestAuthorBuilder):
    def test_negative_id(self):
         self.author_builder.set_id(-3) \
                            .set_identity('Kowalski, Jan')

         self.assertRaises(ValueError, self.author_builder.build)

    def test_null_id(self):
         self.author_builder.set_id(None) \
                            .set_identity('Kowalski, Jan')

         self.assertRaises(ValueError, self.author_builder.build)


class TestAuthorPublicationBuilderIfCorrectShouldBuild(TestAuthorPublicationBuilder):
    def test_build_function(self):
        self.author_publication = self.author_publication_builder.set_author_id(5)\
                                                                 .set_publication_id(6)\
                                                                 .build()
        self.assertEqual(self.author_publication.author_id, 5)
        self.assertEqual(self.author_publication.publication_id, 6)


class TestAuthorPublicationBuilderIfNotCorrectShouldTrowException(TestAuthorPublicationBuilder):
    def test_author_null_id(self):
        self.author_publication_builder.set_author_id(None) \
                                       .set_publication_id(6)

        self.assertRaises(ValueError, self.author_publication_builder.build)

    def test_publication_null_id(self):
        self.author_publication_builder.set_author_id(5) \
                                       .set_publication_id(None)

        self.assertRaises(ValueError, self.author_publication_builder.build)


class TestEmployeeSummaryBuilderIfCorrectShouldBuild(TestEmployeeSummaryBuilder):
    def test_build_function(self):
        self.empolyee_summary = self.employee_summary_builder.set_publication_id(4)\
                                                             .set_publication_source(None)\
                                                             .set_publication_title('Pub1')\
                                                             .set_author_name('Jan, Kowalski')\
                                                             .set_mnisw_points(12)\
                                                             .build()
        self.assertEqual(self.empolyee_summary.publication_id, 4)
        self.assertEqual(self.empolyee_summary.publication_title, 'Pub1')
        self.assertEqual(self.empolyee_summary.publication_source, None)
        self.assertEqual(self.empolyee_summary.author_name, 'Jan, Kowalski')
        self.assertEqual(self.empolyee_summary.mnisw_points, 12)


class TestMniswRecordBuilderIfCorrectShouldBuild(TestMniswRecordBuilder):
    def test_build_function(self):
        self.mnisw_record = self.mnisw_record_builder.set_id(4)\
                                                     .set_points(13)\
                                                     .set_eissn('23-45')\
                                                     .set_issn('14-abc')\
                                                     .set_name('Tech trans')\
                                                     .build()
        self.assertEqual(self.mnisw_record.id, 4)
        self.assertEqual(self.mnisw_record.points, 13)
        self.assertEqual(self.mnisw_record.eissn, '23-45')
        self.assertEqual(self.mnisw_record.issn, '14-abc')
        self.assertEqual(self.mnisw_record.name, 'Tech trans')


class TestMniswRecordBuilderIfNotCorrectShouldTrowException(TestMniswRecordBuilder):
    def test_null_id(self):
        self.mnisw_record_builder.set_id(None)\
                                 .set_points(13)\
                                 .set_eissn('23-45')\
                                 .set_issn('14-abc')\
                                 .set_name('Tech trans')
        self.assertRaises(ValueError, self.mnisw_record_builder.build)


class TestPublicationBuilderIfCorrectShouldBuild(TestPublicationBuilder):
    def test_build_function(self):
        self.publication = self.publication_builder.set_id(3)\
                                                   .set_source('Tech Trans')\
                                                   .set_publisher('PK Wyd')\
                                                   .set_title('Title tech 1')\
                                                   .build()
        self.assertEqual(self.publication.id, 3)
        self.assertEqual(self.publication.source, 'Tech Trans')
        self.assertEqual(self.publication.publisher, 'PK Wyd')
        self.assertEqual(self.publication.title, 'Title tech 1')


class TestPublicationBuilderIfNotCorrectShouldRiseException(TestPublicationBuilder):
    def test_null_id(self):
        self.publication_builder.set_id(None) \
            .set_source('Tech Trans') \
            .set_publisher('PK Wyd') \
            .set_title('Title tech 1')
        self.assertRaises(ValueError, self.publication_builder.build)

    def test_negative_id(self):
        self.publication_builder.set_id(-3) \
            .set_source('Tech Trans') \
            .set_publisher('PK Wyd') \
            .set_title('Title tech 1')
        self.assertRaises(ValueError, self.publication_builder.build)
