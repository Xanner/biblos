from unittest import TestCase

from Models import name_helper
from Models.author import Author
from Models.author_publication import AuthorPublication
from Models.mnisw_record import MniswRecord
from Models.publication import Publication
from Presistence.Factories import reader_simple_factory
from Presistence.Repositories.biblos_data_repository import BiblosDataRepository
from Presistence.Repositories.mnisw_list_repository import MNiSWListRepository



class TestBiblosDataRepository(TestCase):
    def setUp(self):
        self.connection_string = ('Driver={SQL Server Native Client 11.0};'
                                  'Server=.\SQLEXPRESS;'
                                  'Database=Biblos;'
                                  'Trusted_Connection=yes;')
        self.biblos_data_repository = BiblosDataRepository(reader_simple_factory.create_reader(name_helper.Reader.SQL,
                                                                                               self.connection_string),
                                                           reader_simple_factory.create_reader(name_helper.Reader.XML))
        self.sql_helper = reader_simple_factory.create_reader(name_helper.Reader.SQL, self.connection_string)

        self.authors = {'Jan, Test': Author.AuthorBuilder().set_id(9999).set_identity('Jan, Test')
                                                            .build()}
        self.authors_publications = [AuthorPublication.AuthorPublicationBuilder().set_author_id(9999)
                                                                                 .set_publication_id(99990)
                                                                                 .build()]
        self.publications = [Publication.PublicationBuilder().set_id(99990)
                                                             .set_source('Accounting Research Journal')
                                                             .set_publisher('PK Wyd')
                                                             .set_title('Title tech 1')
                                                             .build()]

class TestMniswListRepository(TestCase):
    def setUp(self):
        self.connection_string = ('Driver={SQL Server Native Client 11.0};'
                                  'Server=.\SQLEXPRESS;'
                                  'Database=Biblos;'
                                  'Trusted_Connection=yes;')

        self.mnisw_list_repository = MNiSWListRepository(reader_simple_factory.create_reader(name_helper.Reader.SQL,
                                                                                             self.connection_string),
                                                         reader_simple_factory.create_reader(name_helper.Reader.PDF))

        self.sql_helper = reader_simple_factory.create_reader(name_helper.Reader.SQL, self.connection_string)


class TestBiblosDataRepositoyIfCorrectShouldSaveAndRead(TestBiblosDataRepository):
    def test_save_data_in_database_read_employee_summary_functions(self):
        self.data = (self.publications, self.authors, self.authors_publications)
        self.biblos_data_repository.save_data_in_database(self.data)
        self.return_value = self.biblos_data_repository.get_employee_summary_form_database('Jan, Test')

        self.assertEqual(self.return_value[0].author_name, 'Jan, Test')
        self.assertEqual(self.return_value[0].mnisw_points, 8)
        self.assertEqual(self.return_value[0].publication_id, 99990)
        self.assertEqual(self.return_value[0].publication_source, 'Accounting Research Journal')
        self.assertEqual(self.return_value[0].publication_title, 'Title tech 1')

        self.sql_clean_query_author = "DELETE FROM Authors WHERE AUT_ID = 9999"
        self.sql_clean_query_aut_pub = "DELETE FROM AuthorsPublications WHERE AUP_AuthorID = 9999"
        self.sql_clean_query_publication = "DELETE FROM Publications2 WHERE PUB_ID = 99990"

        self.sql_helper.execute_query(self.sql_clean_query_author, None)
        self.sql_helper.execute_query(self.sql_clean_query_aut_pub, None)
        self.sql_helper.execute_query(self.sql_clean_query_publication, None)
        self.sql_helper.commit()


class TestMniswListRepositoryIfCorrectShouldReadAndSave(TestMniswListRepository):
    def test_read_pdf_save_pdf_get_pdf_fucntions(self):
        self.pdf = 'http://www.bip.nauka.gov.pl/g2/oryginal/2017_01/1ec97396461f9c95e4af247a813246bf.pdf'
        self.data = self.mnisw_list_repository.read_data_from_pdf(self.pdf, (51.684, 40.459, 793.541, 501.659),
                                                      '1-5', True, {'header': None}, 3)
        self.existing_data = self.mnisw_list_repository.read_data_from_database()
        self.assertIsNotNone(self.data)
        self.data_to_save = []
        for val in self.data:
            self.data_to_save.append(MniswRecord.MniswRecordBuilder()
                                                .set_id(val.id)
                                                .set_name(val.name)
                                                .set_eissn('TEST')
                                                .set_issn(val.issn)
                                                .set_points(val.points)
                                                .build())
        self.mnisw_list_repository.save_data_in_database(self.data_to_save)
        self.returned_data = self.mnisw_list_repository.read_data_from_database()
        self.assertEqual(len(self.returned_data), len(self.existing_data) + len(self.data_to_save))

        self.sql_clean_query = "DELETE FROM MNiSWList WHERE MSW_PublicationEISSN = 'TEST'"
        self.sql_helper.execute_query(self.sql_clean_query, None)
        self.sql_helper.commit()

