from unittest import TestCase
import datetime

from Models import name_helper
from Presistence.Factories import reader_simple_factory
from Presistence.Readers.pdf_reader import PDFReader
from Presistence.Readers.xml_reader import XMLReader
from Presistence.Readers.sql_reader import SQLReader


class TestReadersFactory(TestCase):
    def setUp(self):
        self.pdf_reader = None
        self.xml_reader = None
        self.sql_reader = None
        self.connection_string = ('Driver={SQL Server Native Client 11.0};'
                                  'Server=.\SQLEXPRESS;'
                                  'Database=Biblos;'
                                  'Trusted_Connection=yes;')

class TestPDFReader(TestCase):
    def setUp(self):
        self.pdf_reader = reader_simple_factory.create_reader(name_helper.Reader.PDF)

class TestXMLReader(TestCase):
    def setUp(self):
        self.xml_reader = reader_simple_factory.create_reader(name_helper.Reader.XML)

class TestSQLReader(TestCase):
    def setUp(self):
        self.connection_string = ('Driver={SQL Server Native Client 11.0};'
                                  'Server=.\SQLEXPRESS;'
                                  'Database=Biblos;'
                                  'Trusted_Connection=yes;')
        self.sql_reader = reader_simple_factory.create_reader(name_helper.Reader.SQL, self.connection_string)


class TestReadersFactoryIfCorrectShouldReturnReader(TestReadersFactory):
    def test_pdf_factory(self):
        self.pdf_reader = reader_simple_factory.create_reader(name_helper.Reader.PDF)
        self.assertIsInstance(self.pdf_reader, PDFReader)

    def test_xml_factory(self):
        self.xml_reader = reader_simple_factory.create_reader(name_helper.Reader.XML)
        self.assertIsInstance(self.xml_reader, XMLReader)

    def test_sql_factory(self):
        self.sql_reader = reader_simple_factory.create_reader(name_helper.Reader.SQL, self.connection_string)
        self.assertIsInstance(self.sql_reader, SQLReader)

    def test_singleton_sql_type(self):
        self.connection_string = ('Driver={SQL Server Native Client 11.0};'
                                  'Server=.\SQLEXPRESS;'
                                  'Database=Biblos;'
                                  'Trusted_Connection=yes;')
        self.sql_reader1 = reader_simple_factory.create_reader(name_helper.Reader.SQL, self.connection_string)
        self.sql_reader2 = reader_simple_factory.create_reader(name_helper.Reader.SQL, self.connection_string)

        self.assertEqual(self.sql_reader1, self.sql_reader2)

    def test_none_singleton_xml_pdf_type(self):
        self.xml_reader1 = reader_simple_factory.create_reader(name_helper.Reader.XML)
        self.xml_reader2 = reader_simple_factory.create_reader(name_helper.Reader.XML)
        self.pdf_reader1 = reader_simple_factory.create_reader(name_helper.Reader.PDF)
        self.pdf_reader2 = reader_simple_factory.create_reader(name_helper.Reader.PDF)

        self.assertNotEqual(self.xml_reader1, self.xml_reader2)
        self.assertNotEqual(self.pdf_reader1, self.pdf_reader2)


class TestReadersFactoryIfNotCorrectShouldThrowException(TestReadersFactory):
    def test_bad_input(self):
        self.assertRaises(ValueError, reader_simple_factory.create_reader, 'NotExistingReader')


class TestPDFReaderIfCorrectShouldReturnList(TestPDFReader):
    def test_read_pdf_function(self):
        self.pdf = 'http://www.bip.nauka.gov.pl/g2/oryginal/2017_01/1ec97396461f9c95e4af247a813246bf.pdf'
        self.data = self.pdf_reader\
                        .read_pdf(self.pdf, (51.684, 40.459, 793.541, 501.659), '1-8', True, {'header': None}, 3)

        self.assertEqual(len(self.data), 291)

    def test_read_pdf_function2(self):
        self.pdf = 'http://www.bip.nauka.gov.pl/g2/oryginal/2017_01/1ec97396461f9c95e4af247a813246bf.pdf'
        self.data = self.pdf_reader\
                        .read_pdf(self.pdf, (51.684, 40.459, 793.541, 501.659), '1-4', True, {'header': None}, 3)

        self.assertEqual(len(self.data), 154)

class TestXMLReaderIfCorrectShouldReturnCollection(TestXMLReader):
    def test_load_data_function(self):
        self.url = 'http://suw.biblos.pk.edu.pl/suw/src/oai/oai2.php?verb=ListRecords&metadataPrefix=oai_dc'
        self.encoding = 'iso-8859-1'
        self.xml_reader.encode = self.encoding
        self.data = self.xml_reader\
                        .load_data(self.url)
        self.assertEqual(self.data.DEFAULT_BUILDER_FEATURES[0], 'html')
        self.assertIsNotNone(self.data.text)

class TestSQLReaderIFCorrectShouldExecuteQuery(TestSQLReader):
    def test_execute_read_query_time_check(self):
        self.query = 'SELECT CONVERT (date, SYSDATETIME())'
        self.data = self.sql_reader.execute_read_query(self.query, None)
        self.assertEqual(self.data[0][0], datetime.datetime.now().date())

