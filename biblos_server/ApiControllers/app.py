from flask import Flask
from flask import request
from flask import Response
from flask_cors import CORS

import simplejson

from Models.author import Author
from Models.mnisw_record import MniswRecord
from Presistence.unit_of_work import UnitOfWork

app = Flask(__name__)
CORS(app)

oai_init__url = 'http://suw.biblos.pk.edu.pl/suw/src/oai/oai2.php?verb=ListRecords&metadataPrefix=oai_dc'
oai_url = 'http://suw.biblos.pk.edu.pl/suw/src/oai/oai2.php?verb=ListRecords&resumptionToken='
UnitOfWork.connection_string = ('Driver={SQL Server Native Client 11.0};'
                                'Server=.\SQLEXPRESS;'
                                'Database=Biblos;'
                                'Trusted_Connection=yes;')

@app.route('/')
def test_api():
    return 'Api works!'

@app.route('/saveMniswList', methods=['POST'])
def save_mnsiw_data():
    mniswRepository = UnitOfWork.mnisw_list_repository()

    raw_data = request.json
    data = MniswRecord.deserialize_data(raw_data)
    mniswRepository.remove_data_from_database()
    UnitOfWork.save_changes()
    mniswRepository.save_data_in_database(data)
    UnitOfWork.save_changes()

    return Response('Records successfully saved', status=200, mimetype='application/json')


@app.route('/generatePdf', methods=['GET'])
def generate_mnisw_pdf():
    mnisw_repository = UnitOfWork.mnisw_list_repository()

    pdf = request.args.get('pdfAddress', default='', type=str)

    data = mnisw_repository.read_data_from_pdf(pdf, (51.684, 40.459, 793.541, 501.659), '1-80', True, {'header': None}, 3)
    return simplejson.dumps([ob.__dict__ for ob in data], ignore_nan=True)


@app.route('/employeeSummary', methods=['GET'])
def get_employee_summary():
    biblos_data_repository = UnitOfWork.biblos_data_repository()

    employee = request.args.get('employeeName', default='', type=str)
    data = biblos_data_repository.get_employee_summary_form_database(employee)
    return simplejson.dumps([ob.__dict__ for ob in data], ignore_nan=True)


@app.route('/mniswList', methods=['GET'])
def get_mnisw_list():
    mnisw_repository = UnitOfWork.mnisw_list_repository()

    data = mnisw_repository.read_data_from_database()
    return simplejson.dumps([ob.__dict__ for ob in data], ignore_nan=True)


@app.route('/generateOaiData', methods=['GET'])
def generate_and_save_oai_data():
    biblos_data_repository = UnitOfWork.biblos_data_repository()

    raw_data = biblos_data_repository.read_data_from_oai_api(oai_init__url, oai_url)
    biblos_data_repository.save_data_in_database(raw_data)
    UnitOfWork.save_changes()

    return Response('Records successfully saved', status=200, mimetype='application/json')

@app.route('/publications', methods=['GET'])
def get_publications():
    biblos_data_repository = UnitOfWork.biblos_data_repository()
    page = request.args.get('page', default=1, type=int)
    f_source = request.args.get('filterSource', default=None, type=str)

    data = biblos_data_repository.get_publications_from_database(page, f_source)
    return simplejson.dumps(({"data_size": data.count}, [ob.__dict__ for ob in data.publications]), ignore_nan=True)

@app.route('/author', methods=['PUT'])
def edit_author():
    biblos_data_repository = UnitOfWork.biblos_data_repository()

    raw_data = request.json
    data = Author.deserialize_data(raw_data)
    biblos_data_repository.update_author_info(data)
    UnitOfWork.save_changes()

    return Response('Records successfully updated', status=200, mimetype='application/json')

if __name__ == '__main__':
    app.run(debug=True)