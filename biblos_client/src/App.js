import React from 'react';
import { withRouter , Route, Switch } from 'react-router-dom';
import { Layout } from 'antd';

import './App.css';
import 'antd/dist/antd.css';
import './biblos_style.css';

import SiderMenu from './Components/sider_menu';

import ScientificEmployeesPage from './Pages/scientific_employees_page';
import PublicationsPage from './Pages/publications_page';
import PunctationPage from './Pages/punctation_page';

function App() {
  return (
    <div className="App">
      <Layout style={{ minHeight: '100vh' }}>
          <SiderMenu />
          <Switch>
            <Route exact path='/' component={ScientificEmployeesPage}/>
            <Route exact path='/scientific-employees' component={ScientificEmployeesPage}/>
            <Route exact path='/publications' component={PublicationsPage}/>
            <Route exact path='/punctation' component={PunctationPage}/>
          </Switch>
      </Layout>
    </div>
  );
}

export default withRouter(App);
