let instance = null;

class Observable {
  constructor() {
    if (!instance) {
      instance = this;
    }
    this.observers = [];
    return instance;
  }

  subscribe(event, listener) {
    if (!this.observers.find(observer => observer.event === event)) {
      this.observers.push({ event, listener });
    }
  }

  unsubscribe(event) {
    this.observers = this.observers.filter(
      subscriber => subscriber.event !== event,
    );
  }

  notify(event, data) {
    this.observers.forEach(
      observer => observer.event === event && observer.listener(data),
    );
  }
}

export default new Observable();
