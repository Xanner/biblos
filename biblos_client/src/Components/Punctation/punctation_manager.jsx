import React from 'react';
import axios from 'axios';
import { isEmpty } from 'lodash';
import { Button, notification, Modal, Input, Spin, Popconfirm } from 'antd';

const openNotificationWithIcon = (type, title, desc) => {
  notification[type]({
    message: title,
    description: desc,
  });
};

class PunctationManager extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      isVisibleSecond: false,
      addressLink: null,
      hasLoaded: true,
    };
  }

  showModal = () => {
    this.setState({
      isVisible: true,
    });
  };

  handleLoadList = () => {
    if (!isEmpty(this.state.addressLink)) {
      this.setState({ hasLoaded: false }, () => {
        axios({
          method: 'get',
          url: `http://127.0.0.1:5000/generatePdf?pdfAddress=${
            this.state.addressLink
          }`,
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
          data: this.props.dataSource,
        }).then(resp => {
          if (resp.status === 200) {
            openNotificationWithIcon(
              'success',
              'Sukces',
              'Wczytano dane z podanego linku.',
            );
            axios({
              method: 'post',
              url: `http://127.0.0.1:5000/saveMniswList`,
              headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
              },
              data: resp.data,
            }).then(resp => {
              if (resp.status === 200) {
                openNotificationWithIcon(
                  'success',
                  'Sukces',
                  'Nowa lista punktacji została zapisana.',
                );
                this.setState(
                  {
                    isVisible: false,
                  },
                  () => {
                    this.props.getMniswList();
                  },
                );
              }
            });
          }
        });
      });
    }
  };

  handleCancelList = e => {
    this.setState({
      isVisible: false,
      addressLink: null,
    });
  };

  handleSaveList = () => {
    axios({
      method: 'post',
      url: `http://127.0.0.1:5000/saveMniswList`,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      },
      data: this.props.dataSource,
    }).then(resp => {
      if (resp.status === 200) {
        openNotificationWithIcon(
          'success',
          'Sukces',
          'Nowa lista punktacji została zapisana.',
        );
        this.props.getMniswList();
      }
    });
  };

  handleAddressLinkChange = value => this.setState({ addressLink: value });

  handleLoadEmployees = () => {
    this.setState({isVisibleSecond: true}, () => {
      axios({
        method: 'get',
        url: `http://127.0.0.1:5000/generateOaiData`,
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
      }).then(resp => {
        if (resp.status === 200) {
          openNotificationWithIcon(
            'success',
            'Sukces',
            'Dane z Biblios zostały wczytane',
          );
          this.setState({isVisibleSecond: false})
        }
      });
    });
  }

  render() {
    return (
      <React.Fragment>
        <Button
          onClick={this.props.handleAdd}
          type="primary"
          shape="round"
          icon="plus"
          style={{
            zIndex: '10000',
            left: 20,
            bottom: 5,
            position: 'absolute',
            backgroundColor: '#545871',
          }}
        >
          Dodaj kategorię
        </Button>
        <Button
          onClick={this.showModal}
          type="primary"
          shape="round"
          icon="upload"
          style={{
            zIndex: '10000',
            left: 200,
            bottom: 5,
            position: 'absolute',
            backgroundColor: '#545871',
          }}
        >
          Wczytaj listę
        </Button>
        <Button
          onClick={this.handleSaveList}
          type="primary"
          shape="round"
          icon="save"
          style={{
            zIndex: '10000',
            left: 355,
            bottom: 5,
            position: 'absolute',
            backgroundColor: '#545871',
          }}
        >
          Zapisz listę
        </Button>
        <Popconfirm
            title="Czy na pewno chcesz załadować dane z Biblios?"
            onConfirm={() => this.handleLoadEmployees()}
            okText="Tak"
            cancelText="Anuluj"
          >
          <Button
            type="primary"
            shape="round"
            icon="save"
            style={{
              zIndex: '10000',
              left: 500,
              bottom: 5,
              position: 'absolute',
              backgroundColor: '#545871',
            }}
          >
            Załaduj dane Biblios
          </Button>
        </Popconfirm>
        <Modal
          title="Trwa wczytywanie danych z Biblios"
          visible={this.state.isVisibleSecond}
          cancelText="Anuluj"
          okButtonProps={{ disabled: true }}
          cancelButtonProps={{ disabled: true }}
        >
          <Spin /> To może potrwać kilka minut.
        </Modal>
        <Modal
          title="Wczytaj nowa punktację"
          visible={this.state.isVisible}
          onOk={this.handleLoadList}
          cancelText="Anuluj"
          okText={!this.state.hasLoaded ? <Spin /> : 'Wczytaj'}
          onCancel={this.handleCancelList}
        >
          <Input
            placeholder="Podaj link do pdf'a"
            value={this.state.addressLink}
            onChange={e => this.handleAddressLinkChange(e.target.value)}
          />
        </Modal>
      </React.Fragment>
    );
  }
}

export default PunctationManager;
