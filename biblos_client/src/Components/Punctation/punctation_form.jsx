import React from 'react';
import axios from 'axios';
import { Table, Input, Button, Popconfirm, Form, Icon, Spin } from 'antd';
import Highlighter from 'react-highlight-words';
import { EditableCell, EditableRow } from './editable_table';
import PunctationManager from './punctation_manager';

const EditableFormRow = Form.create()(EditableRow);

export default class PunctationForm extends React.Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: 'Kategoria',
        dataIndex: 'name',
        width: '50%',
        editable: true,
        ...this.getColumnSearchProps('name'),
      },
      {
        title: 'Nr ISSN',
        dataIndex: 'issn',
        width: '15%',
        editable: true,
        ...this.getColumnSearchProps('issn'),
      },
      {
        title: 'EISSN',
        dataIndex: 'eissn',
        width: '15%',
        editable: true,
      },
      {
        title: 'Liczba punktów',
        dataIndex: 'points',
        width: '10%',
        editable: true,
      },
      {
        title: '',
        dataIndex: 'operation',
        width: '5%',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm
              title="Czy na pewno chcesz usunąć ten rekord?"
              onConfirm={() => this.handleDelete(record.id)}
              okText="Tak"
              cancelText="Anuluj"
            >
              <Icon type="close" />
            </Popconfirm>
          ) : null,
      },
    ];

    this.state = {
      dataSource: [],
      searchText: '',
      hasData: false,
      isVisible: false,
      count: null,
    };
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Wpisz szukaną frazę`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Szukaj
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Wyczyść
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) => {
      return (
        record[dataIndex] &&
        record[dataIndex]
          .toString()
          .toLowerCase()
          .includes(value.toLowerCase())
      );
    },
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text ? text.toString() : ''}
      />
    ),
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  handleDelete = key => {
    const dataSource = [...this.state.dataSource];
    this.setState({ dataSource: dataSource.filter(item => item.id !== key) });
  };

  handleAdd = () => {
    const { count, dataSource } = this.state;
    const newData = {
      id: count + 1,
      name: null,
      eissn: null,
      issn: null,
      points: 0,
    };
    this.setState({
      dataSource: [...dataSource, newData],
      count: count + 1,
    });
  };

  handleSave = row => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex(item => row.id === item.id);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData });
  };

  componentDidMount() {
    this.getMniswList();
  }

  getMniswList = () => {
    axios({
      method: 'get',
      url: `http://127.0.0.1:5000/mniswList`,
      headers: { 'Access-Control-Allow-Origin': '*' },
    }).then(resp => {
      if (resp.data) {
        this.setState({
          dataSource: resp.data.map(m => ({
            id: m.id,
            eissn: m.eissn,
            issn: m.issn,
            name: m.name,
            points: m.points,
          })),
          hasData: true,
          count: resp.data.length,
        });
      }
    });
  };

  render() {
    const { dataSource } = this.state;
    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };
    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });
    if (!this.state.hasData) {
      return (
        <div style={{ paddingTop: '100px' }} className="ant-modal-centered">
          <Spin size="large" />
        </div>
      );
    }

    return (
      <div>
        <Table
          locale={{ emptyText: 'Brak wyników.' }}
          rowKey={'id'}
          scroll={{ y: 600 }}
          bordered
          components={components}
          rowClassName={() => 'editable-row'}
          dataSource={dataSource}
          columns={columns}
        />
        <PunctationManager
          handleAdd={this.handleAdd}
          dataSource={this.state.dataSource}
          getMniswList={this.getMniswList}
        />
      </div>
    );
  }
}
