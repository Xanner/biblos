import React from 'react';
import { Input } from 'antd';

import Observable from '../observable';

class SerachBar extends React.PureComponent {
  render() { 
    return ( 
      <Input
        size="large"
        style={{width: '80%', borderRadius: '100px'}}
        placeholder={this.props.placeholderText}
        onChange={e => Observable.notify(this.props.notifier, e.target.value)}
      />
    );
  }
}
 
export default SerachBar;