import React from 'react';
import { Avatar, Col, Row, Icon, Typography, Spin } from 'antd';
import PublicationsForm from '../Publications/publications_form';
import EmployeeManager from './employee_manager';

const { Text } = Typography;

class ScientificEmployeeDetailsForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      employee: null,
      hasData: false,
    };
  }

  componentDidMount() {
    this.setState({ employee: this.props.employee, hasData: true });
  }

  componentWillReceiveProps() {
    this.setState({ hasData: false }, () => {
      this.setState({ employee: this.props.employee, hasData: true });
    });
  }

  render() {
    if (!this.state.hasData) {
      return (
        <div style={{ paddingTop: '100px' }} className="ant-modal-centered">
          <Spin size="large" />
        </div>
      );
    }

    const { name, publications, points, logo, address } = this.state.employee;

    return (
      <div>
        <Row type="flex" style={{ padding: '0 60px 0 60px' }}>
          <Col span={18} style={{ textAlign: 'left' }}>
            <Text style={{ color: '#9597A6', fontSize: 24 }}>{name}</Text>
          </Col>
          <Col span={6}>
            <h4>
              <Icon type="star" style={{ color: '#B8817D' }} theme="filled" />{' '}
              &nbsp;
              <Text style={{ color: '#9597A6' }}>
                {points} {points <= 4 || points === 0 ? 'punkty' : 'punktów'}
              </Text>
            </h4>
            <h4>
              <Icon type="book" style={{ color: '#B8817D' }} theme="filled" />{' '}
              &nbsp;
              <Text style={{ color: '#9597A6' }}>
                {publications.length}{' '}
                {publications.length === 1
                  ? 'publikacja'
                  : publications.length <= 4 || publications.length === 0
                  ? 'publikacje'
                  : 'publikacji'}
              </Text>
            </h4>
          </Col>
        </Row>
        <hr />
        <Row type="flex" style={{ padding: '20px 80px 0 80px' }}>
          <Col span={12} style={{ textAlign: 'left' }}>
            <Row>
              <Col span={10}>
                <Icon
                  type="global"
                  style={{ color: '#EBD0CE', fontSize: 60 }}
                />{' '}
              </Col>
              <Col span={14}>{address || 'Brak adresu.'}</Col>
            </Row>
          </Col>
          <Col span={12} style={{ textAlign: 'right' }}>
            <Avatar size={140} icon="user" src={logo} />
          </Col>
        </Row>
        <EmployeeManager employee={this.state.employee} />
        <hr />
        <Row type="flex" style={{ padding: '20px 80px 20px 80px' }}>
          <Col span={24} style={{ textAlign: 'left' }}>
            <Text style={{ color: '#9597A6', fontSize: 18 }}>
              Lista publikacji
            </Text>
            <PublicationsForm publications={publications} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default ScientificEmployeeDetailsForm;
