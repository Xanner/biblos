import React from 'react';
import { Col } from 'antd';

import ScientificEmployeesList from './scientific_employees_list';

class ScientificEmployeesView extends React.PureComponent {
  render() { 
    return ( 
      <Col style={{paddingTop: '5px', textAlign: 'left'}}>
        <hr />
        <p style={{color: '#9597A6', fontSize: '11px', padding: '5px 0 0 40px'}}><strong>WYNIKI WYSZUKIWANIA</strong></p>
        <hr />
        <ScientificEmployeesList />
      </Col>
     );
  }
}
 
export default ScientificEmployeesView;