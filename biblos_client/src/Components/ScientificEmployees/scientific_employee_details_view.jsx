import React from 'react';
import { Empty } from 'antd';

import ScientificEmployeeDetailsForm from './scientific_employee_details_form';
import Observable from '../../observable';

class ScientificEmployeeDetailsView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      employee: {
        name: null,
      },
    };
  }

  componentDidMount() {
    Observable.subscribe('employeDetails', employee =>
      this.handleChangeEmployee(employee),
    );
  }

  componentWillUnmount() {
    Observable.unsubscribe('employeDetails');
  }

  handleChangeEmployee = employee => {
    this.setState({ employee: employee });
  };

  render() {
    return (
      <React.Fragment>
        {!this.state.employee.name ? (
          <Empty
            style={{marginTop: '300px'}}
            description={
              <span>Wybierz pracownika, aby wyświetlić szczegóły.</span>
            }
          />
        ) : (
          <div>
            <ScientificEmployeeDetailsForm employee={this.state.employee} />
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default ScientificEmployeeDetailsView;
