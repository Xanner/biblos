import React from 'react';
import axios from 'axios';
import { Button, Typography, notification, Modal, Input, Spin } from 'antd';
import { CopyToClipboard } from 'react-copy-to-clipboard';

import Observable from '../../observable';

const { Text } = Typography;

class EmployeeManager extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      employee: {
        name: null,
        email: null,
        logo: null,
        address: null,
      },
      isVisible: false,
      hasLoaded: true,
    };
  }

  showNotification = () => {
    if (this.state.employee.email) {
      notification['success']({
        message: 'Email został skopiowany do schowka.',
      });
    } else {
      notification['warning']({
        message: 'Pracownik nie ma określonego adresu e-mail.',
      });
    }
  };

  showModal = () => {
    this.setState({
      isVisible: true,
    });
  };

  handleCancel = () => {
    this.setState({
      isVisible: false,
    });
  };

  handleEmployeeChange = (key, value) => {
    this.setState(prevState => ({
      employee: {
        ...prevState.employee,
        [key]: value,
      },
    }));
  };

  handleUpdateEmployee = () => {
    axios({
      method: 'put',
      url: `http://127.0.0.1:5000/author`,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      },
      data: {
        author_id: this.state.employee.id,
        author_name: this.state.employee.name,
        author_email: this.state.employee.email,
        author_address: this.state.employee.address,
        author_logo: this.state.employee.logo,
      }
    }).then(resp => {
      if (resp.status === 200) {
        notification['success']({
          message: 'Pracownik został zaktualizowany.',
        });
        this.setState({isVisible: false});
        Observable.notify('updateEmployee', this.state.employee);
      }
    });
  };

  componentDidMount() {
    this.setState({ employee: this.props.employee });
  }

  render() {
    return (
      <div>
        <CopyToClipboard
          text={this.state.employee.email}
          onCopy={() => this.showNotification()}
        >
          <Button
            style={{ marginTop: '50px' }}
            type="primary"
            shape="round"
            icon="mail"
            size="large"
          >
            <Text style={{ color: '#545871', fontSize: 14 }}>E-mail</Text>
          </Button>
        </CopyToClipboard>
        &nbsp;
        <Button
          style={{ marginTop: '50px' }}
          type="primary"
          shape="round"
          icon="edit"
          size="large"
          onClick={this.showModal}
        >
          <Text style={{ color: '#545871', fontSize: 14 }}>Edytuj</Text>
        </Button>
        <Modal
          title="Dane pracownika"
          visible={this.state.isVisible}
          onOk={this.handleUpdateEmployee}
          onCancel={this.handleCancel}
          okText={!this.state.hasLoaded ? <Spin /> : 'Zapisz'}
          cancelText="Anuluj"
        >
          <div style={{ marginBottom: 10 }}>
            <Input
              placeholder="Imię i nazwisko"
              disabled={true}
              value={this.state.employee.name}
              onChange={e => this.handleEmployeeChange('name', e.target.value)}
            />
          </div>
          <div style={{ marginBottom: 10 }}>
            <Input
              placeholder="E-mail"
              value={this.state.employee.email}
              onChange={e => this.handleEmployeeChange('email', e.target.value)}
            />
          </div>
          <div style={{ marginBottom: 10 }}>
            <Input
              placeholder="Adres"
              value={this.state.employee.address}
              onChange={e =>
                this.handleEmployeeChange('address', e.target.value)
              }
            />
          </div>
          <Input
            placeholder="Link do zdjęcia"
            value={this.state.employee.logo}
            onChange={e =>
              this.handleEmployeeChange('logo', e.target.value)
            }
          />
        </Modal>
      </div>
    );
  }
}

export default EmployeeManager;
