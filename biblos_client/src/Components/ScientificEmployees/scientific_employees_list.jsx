import React from 'react';
import axios from 'axios';
import { sumBy, groupBy } from 'lodash';
import { debounce } from 'throttle-debounce';
import InfiniteScroll from 'react-infinite-scroller';
import { List, Avatar, Col, Spin, Icon, Typography } from 'antd';

import Observable from '../../observable';

const { Text } = Typography;

class ScientificEmployeesList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      hasData: false,
      selectedItem: null,
    };
  }

  handleClick = item => {
    this.setState({ selectedItem: item.name }, () =>
      Observable.notify('employeDetails', item),
    );
  };

  componentDidMount() {
    Observable.subscribe('searchEmployee', employeeName =>
      this.getEmployee(employeeName),
    );
    Observable.subscribe('updateEmployee', employee => {
      this.updateEmployee(employee);
    });
    this.setState({hasData: true});
  }

  updateEmployee = employee => {
    Observable.notify('employeDetails', employee);
  }

  componentWillUnmount() {
    Observable.unsubscribe('searchEmployee');
    Observable.unsubscribe('updateEmployee');
  }

  getEmployee = debounce(300, employeeName => {
    if (employeeName.length >= 3) {
      this.setState({ hasData: false }, () => {
        axios({
          method: 'get',
          url: `http://127.0.0.1:5000/employeeSummary?employeeName=${employeeName}`,
          headers: { 'Access-Control-Allow-Origin': '*' },
        }).then(resp => {
          if (resp.data) {
            const employees = groupBy(resp.data, 'author_name');
            const employeesKeys = Object.keys(employees);
            this.setState({
              data: employeesKeys.map(e => ({
                id: employees[e][0].author_id,
                name: e.replace(',', ''),
                address: employees[e][0].author_address,
                email: employees[e][0].author_email,
                logo: employees[e][0].author_logo,
                publications: employees[e],
                points: sumBy(employees[e], 'mnisw_points'),
              })),
              hasData: true,
            });
          }
        });
      });
    }
  });

  render() {
    if (!this.state.hasData) {
      return (
        <div style={{ paddingTop: '100px' }} className="ant-modal-centered">
          <Spin size="large" />
        </div>
      );
    }

    return (
      <div className="infinite-container" style={{ height: '83.5vh' }}>
        <InfiniteScroll loadMore={() => false}>
          <List
            style={{ cursor: 'pointer' }}
            itemLayout="horizontal"
            locale={{ emptyText: 'Brak wyników.' }}
            dataSource={this.state.data}
            renderItem={item => (
              <List.Item
                onClick={() => this.handleClick(item)}
                style={{
                  backgroundColor:
                    this.state.selectedItem === item.name
                      ? '#FFFFFF'
                      : '#F0F1F7',
                }}
              >
                <Col span={16}>
                  <h2>{item.name}</h2>
                  <h4>
                    <Icon
                      type="star"
                      style={{ color: '#B8817D' }}
                      theme="filled"
                    />{' '}
                    &nbsp;
                    <Text style={{ color: '#9597A6' }}>
                      {item.points}{' '}
                      {item.points <= 4 || item.points === 0
                        ? 'punkty'
                        : 'punktów'}
                    </Text>
                  </h4>
                  <h4>
                    <Icon
                      type="book"
                      style={{ color: '#B8817D' }}
                      theme="filled"
                    />{' '}
                    &nbsp;
                    <Text style={{ color: '#9597A6' }}>
                      {item.publications.length}{' '}
                      {item.publications.length === 1
                        ? 'publikacja'
                        : item.publications.length <= 4 ||
                          item.publications.length === 0
                        ? 'publikacje'
                        : 'publikacji'}
                    </Text>
                  </h4>
                </Col>
                <Col>
                  <Avatar size={100} icon="user" src={item.logo} />
                </Col>
              </List.Item>
            )}
          >
          </List>
        </InfiniteScroll>
      </div>
    );
  }
}

export default ScientificEmployeesList;
