import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu, Icon } from 'antd';

import Observable from '../observable';

const { Sider } = Layout;

class SiderMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      logoSrc: require('../biblos_logo.svg'),
      currentKey: ['1'],
    };
  }

  onCollapse = collapsed => {
    this.setState({
      collapsed,
      logoSrc: collapsed
        ? require('../biblos_shorter_logo.svg')
        : require('../biblos_logo.svg'),
    });
  };

  componentWillMount() {
    Observable.subscribe('updateMenu', path => this.updateMenuPath(path));
  }

  componentWillUnmount() {
    Observable.unsubscribe('updateMenu');
  }
  
  updateMenuPath = path => {
    if(path === '/scientific-employees') this.setState({currentKey: ['1']});
    if(path === '/publications') this.setState({currentKey: ['2']});
    if(path === '/punctation') this.setState({currentKey: ['3']});
  }

  handleChangeKey = key => this.setState({currentKey: [key]});

  render() {
    return (
      <Sider
        theme="light"
        collapsible
        collapsed={this.state.collapsed}
        onCollapse={this.onCollapse}
        width={300}
        collapsedWidth={100}
      >
        <img alt="logo" width={130} src={this.state.logoSrc} />
        <Menu defaultSelectedKeys={['1']} selectedKeys={this.state.currentKey} mode="inline" onClick={e => this.handleChangeKey(e.key)}>
          <Menu.Item key="1">
            <Icon type="unordered-list" />
            <span>Pracownicy</span>
            <Link to="/scientific-employees" />
          </Menu.Item>
          <Menu.Item key="2">
            <Icon type="project" />
            <span>Publikacje</span>
            <Link to="/publications" />
          </Menu.Item>
          <Menu.Item key="3">
            <Icon type="crown" />
            <span>Punktacja</span>
            <Link to="/punctation" />
          </Menu.Item>
        </Menu>
      </Sider>
    );
  }
}

export default SiderMenu;
