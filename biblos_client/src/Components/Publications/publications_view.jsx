import React from 'react';
import axios from 'axios';
import { isEmpty } from 'lodash';
import { debounce } from 'throttle-debounce';
import { Table, Input, Button, Icon, Spin } from 'antd';
import Highlighter from 'react-highlight-words';

import Observable from '../../observable';
class PublicationsView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      dataSource: null,
      currentPage: 1,
      count: null,
      hasData: false,
      categoryQuery: null,
    };
  }

  componentDidMount() {
    Observable.subscribe('searchCategory', category =>
      this.setState(
        { categoryQuery: !isEmpty(category) && `&filterSource=${category}` },
        () => this.getPublications(),
      ),
    );
    this.getPublications();
  }

  componentWillUnmount() {
    Observable.unsubscribe('searchCategory');
  }

  getPublications = debounce(300, () => {
    axios({
      method: 'get',
      url: `http://127.0.0.1:5000/publications?page=${
        this.state.currentPage
      }${this.state.categoryQuery || ''}`,
      headers: { 'Access-Control-Allow-Origin': '*' },
    }).then(resp => {
      if (resp.data) {
        this.setState({
          dataSource: resp.data[1].map(p => ({
            id: p.id,
            title: p.title,
            publisher: p.publisher,
            source: p.source,
          })),
          hasData: true,
          count: resp.data[0].data_size,
        });
      }
    });
  });

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={'Wpisz szukaną frazę'}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Szukaj
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Wyczyść
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex] &&
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text ? text.toString() : ''}
      />
    ),
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  onPageChange = pageNumber =>
    this.setState({ currentPage: pageNumber }, () => this.getPublications());

  render() {
    if (!this.state.hasData) {
      return (
        <div style={{ paddingTop: '100px' }} className="ant-modal-centered">
          <Spin size="large" />
        </div>
      );
    }

    const columns = [
      {
        title: 'Tytuł',
        dataIndex: 'title',
        key: 'title',
        width: '50%',
        ...this.getColumnSearchProps('title'),
      },
      {
        title: 'Wydawca',
        dataIndex: 'publisher',
        key: 'publisher',
        width: '25%',
        ...this.getColumnSearchProps('publisher'),
      },
      {
        title: 'Kategoria',
        dataIndex: 'source',
        key: 'source',
        ...this.getColumnSearchProps('source'),
      },
    ];
    return (
      <Table
        style={{ paddingTop: '30px' }}
        scroll={{ y: 600 }}
        pagination={{
          defaultCurrent: this.state.currentPage,
          onChange: pageNumber => this.onPageChange(pageNumber),
          total: this.state.count,
          pageSize: 50,
        }}
        locale={{ emptyText: 'Brak wyników' }}
        rowKey={'id'}
        columns={columns}
        dataSource={this.state.dataSource}
      />
    );
  }
}

export default PublicationsView;
