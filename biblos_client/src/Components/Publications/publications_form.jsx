import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { List, Icon } from 'antd';

function PublicationsForm(props) {
  return (
    <div className="infinite-container" style={{ height: '48vh' }}>
      <InfiniteScroll loadMore={() => false}>
        <List
          itemLayout="horizontal"
          locale={{ emptyText: 'Brak wyników.' }}
          style={{ cursor: 'pointer' }}
          dataSource={props.publications}
          renderItem={item => (
            <List.Item>
              <List.Item.Meta
                avatar={
                  <Icon
                    theme="filled"
                    type="calendar"
                    style={{ color: '#EBD0CE', fontSize: 40 }}
                  />
                }
                title={item.publication_title}
                description={
                  (item.publication_source || 'Brak kategori') + ` - punktów: ${item.mnisw_points || 0}`
                }
              />
            </List.Item>
          )}
        />
      </InfiniteScroll>
    </div>
  );
}

export default PublicationsForm;
