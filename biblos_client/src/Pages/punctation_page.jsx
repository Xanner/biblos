import React from 'react';
import { Layout, Col, Row } from 'antd';

import PunctationForm from '../Components/Punctation/punctation_form';
import Observable from '../observable';

const { Content } = Layout;

class PunctationPage extends React.PureComponent {
  render() {
    Observable.notify('updateMenu', this.props.location.pathname);
    return (
      <Content>
        <Row>
          <Col
            style={{
              height: '100vh',
              backgroundColor: '#F0F1F7',
              padding: '20px'
            }}
          >
            <div
              className="scrollable-container"
              ref={node => {
                this.container = node;
              }}
            >
              <PunctationForm />
            </div>
          </Col>
        </Row>
      </Content>
    );
  }
}

export default PunctationPage;
