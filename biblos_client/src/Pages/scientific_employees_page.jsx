import React from 'react';
import { Layout, Col, Row } from 'antd';

import SearchBar from '../Components/search_bar';
import ScientificEmployeesView from '../Components/ScientificEmployees/scientific_employees_view';
import ScientificEmployeeDetailsView from '../Components/ScientificEmployees/scientific_employee_details_view';
import Observable from '../observable';

const { Content } = Layout;

class ScientificEmployeesPage extends React.PureComponent {
  render() {
    Observable.notify('updateMenu', this.props.location.pathname);
    return (
      <Content>
        <Row>
          <Col
            span={10}
            style={{
              height: '100vh',
              backgroundColor: '#F0F1F7',
              paddingTop: '20px',
            }}
          >
            <SearchBar 
              notifier={'searchEmployee'}
              placeholderText="Wyszukaj pracownika" 
            />
            <ScientificEmployeesView />
          </Col>
          <Col
            span={14}
            style={{
              height: '100vh',
              backgroundColor: '#FFFFFF',
              paddingTop: '20px',
            }}
          >
            <ScientificEmployeeDetailsView />
          </Col>
        </Row>
      </Content>
    );
  }
}

export default ScientificEmployeesPage;
