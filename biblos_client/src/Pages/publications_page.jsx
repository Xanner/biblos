import React from 'react';
import { Layout, Col, Row } from 'antd';

import PublicationsView from '../Components/Publications/publications_view';
import SearchBar from '../Components/search_bar';
import Observable from '../observable';

const { Content } = Layout;

class PublicationsPage extends React.PureComponent {
  render() {
    Observable.notify('updateMenu', this.props.location.pathname);
    return (
      <Content>
        <Row>
          <Col
            style={{
              height: '100vh',
              backgroundColor: '#F0F1F7',
              padding: '20px',
            }}
          >
            <SearchBar
              notifier={'searchCategory'}
              placeholderText="Wyszukaj kategorię"
            />
            <div
              className="scrollable-container"
              ref={node => {
                this.container = node;
              }}
            >
              <PublicationsView />
            </div>
          </Col>
        </Row>
      </Content>
    );
  }
}

export default PublicationsPage;
